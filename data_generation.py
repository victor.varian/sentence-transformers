import pickle
import pandas as pd

from sklearn.utils import shuffle

df = pd.read_csv('input/similar_items.csv', sep='\t')
df = df[df['title'].notna()]
df.reset_index(inplace=True)

num_group = len(df.groupby(['class_id']))
print("Number of group:", num_group)

# remove class_id that contains only one example
num_title_category = df.groupby(['class_id']).count()['title']
one_title_example = []
for idx in num_title_category.index:
    if (num_title_category[idx] == 1):
        df = df[df["class_id"] != idx]
        one_title_example.append(idx)

df.reset_index(inplace=True)
del df['level_0']
del df['index']

num_group = len(df.groupby(['class_id']))
print("[AFTER] Number of group:", num_group)

title_group = df.groupby(['class_id'])['title'].apply(list).values
new_result = pd.DataFrame({"class_id": range(len(title_group)), "candidates": title_group})
result = shuffle(new_result, random_state=42)
result.reset_index(inplace=True)

train_pkl = result[:300000]["candidates"].values
test_pkl = result[300000:]["candidates"].values

# Saving
result[:300000].to_csv('input/train_dataset.csv', index=False)
result[300000:].to_csv('input/test_dataset.csv', index=False)

with open('input/train_pickle.pkl', 'wb') as fp:
    pickle.dump(train_pkl, fp)

with open('input/test_pickle.pkl', 'wb') as fp:
    pickle.dump(test_pkl, fp)


###########
# Loading #
###########
#
# train = pd.read_csv('input/train_dataset.csv')
# test = pd.read_csv('input/test_dataset.csv')
#
# with open('train_pickle.pkl', 'rb') as fp:
#     train_pkl_loaded = pickle.load(fp)
#
# with open('test_pickle.pkl', 'rb') as fp:
#     test_pkl_loaded = pickle.load(fp)
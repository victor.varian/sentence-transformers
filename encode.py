import sys
import pickle

from typing import Union, Iterable
from sentence_transformers import SentenceTransformer

def type_checker(reader: Union[str, Iterable[str]], output_path=None):
    try:
        if (type(reader) is str):
            if (not output_path):
                raise ValueError('output_path is None')

            data = open(reader, "r")
            data.close()
        else:
            iter(reader)
    except Exception as ex:
        print(ex)
        return False

    return True


def encode(reader: Union[str, Iterable[str]], model_path: str, output_path=None):
    is_meeting_criteria = type_checker(reader, output_path)
    if (not is_meeting_criteria):
        raise ValueError("Unable to encode with the given parameters")

    # load model
    model = SentenceTransformer(model_path) # check whether you have added the tokenizer.json to the model
    print("Model loaded successfully")
    
    if (type(reader) is str):
        data = open(reader, "r")
        sentences = data.read().split("\n")
    
        embeddings = model.encode(sentences)
        with open(output_path, 'wb') as handle:
            pickle.dump(embeddings, handle)

        data.close()
        return
    else:
        sentences = list(reader)
        embeddings = model.encode(sentences)
        return embeddings
        

if __name__ == "__main__":
    if (len(sys.argv) < 3 or len(sys.argv) > 4):
        raise ValueError('Too many or little arguments passed, it should be 4 or 5')

    reader = sys.argv[1]
    model_path = sys.argv[2]
    output_path = None

    if (len(sys.argv) == 4):
        output_path = sys.argv[3]

    encode(reader, model_path, output_path)
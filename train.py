import pandas as pd
import numpy as np
import random
import torch
import argparse
import pickle

from torch.utils.data import DataLoader
from sentence_transformers import SentencesDataset, SentenceTransformer, InputExample, losses, models
from sentence_transformers.evaluation import BinaryClassificationEvaluator
from tqdm import tqdm

random.seed(88)
np.random.seed(88)
torch.manual_seed(88)
torch.cuda.manual_seed_all(88)
torch.backends.cudnn.deterministic = True


parser = argparse.ArgumentParser()
parser.add_argument(
    '--pre', type=str, default='input/shopee-roberta-small',
    help='Specify your pretrained model name, your file should be in input folder'
)

parser.add_argument(
    '--out', type=str, default='output/my_final_model',
    help='Specify your output model name, your file output will be in output folder'
)

parser.add_argument(
    '--epochs', type=int, default=8,
    help='Specify how many epochs your model run'
)

parser.add_argument(
    '--eval_steps', type=int, default=10000,
    help="Run validation for every eval_steps training dataset. Put a very large value to remove eval step"
)

parser.add_argument(
    '--ratio', type=int, default=10,
    help="Ratio negative examples to positive examples in integer"
)

parser.add_argument(
    '--sample_train', type=int, default=60000,
    help="Number of class_id you want to sample out of your train dataset (max_value)"
)

parser.add_argument(
    '--sample_test', type=int, default=10000,
    help="Number of class_id you want to sample out of your test dataset (max_value)"
)


args = parser.parse_args()
word_embedding_model = models.Transformer(args.pre, max_seq_length=384)
pooling_model = models.Pooling(word_embedding_model.get_word_embedding_dimension())
model = SentenceTransformer(modules=[word_embedding_model, pooling_model])
print("Model load successfully")

with open('input/train_pickle.pkl', 'rb') as fp:
    train_pkl = pickle.load(fp)

with open('input/test_pickle.pkl', 'rb') as fp:
    test_pkl = pickle.load(fp)

print("Number of train class_id:", len(train_pkl))
print("Number of test class_id:", len(test_pkl))

if (args.sample_train >= len(train_pkl)):
    args.sample_train = len(train_pkl)
    print("Sample train value is adjust to maximum class_id of train dataset")

if (args.sample_test >= len(test_pkl)):
    args.sample_test = len(test_pkl)
    print("Sample test value is adjust to maximum class_id of test dataset")


train_class_id = random.sample(range(len(train_pkl)), args.sample_train)
test_class_id = random.sample(range(len(test_pkl)), args.sample_test)

train_examples = []
test_examples = []

for i in tqdm(train_class_id):
    # [TRAIN] sample 2 titles from same class_id
    numbers = random.sample(range(len(train_pkl[i])), 2)
    first_title = train_pkl[i][numbers[0]]
    second_title = train_pkl[i][numbers[1]]
    train_examples.append(InputExample(texts=[first_title, second_title], label=1.0))
    
    # [TEST]
    numbers = random.sample(range(len(train_pkl)), args.ratio)
    while (i in numbers):
        numbers = random.sample(range(len(train_pkl)), args.ratio)
    
    for x in range(args.ratio):
        title_num_1 = random.sample(range(len(train_pkl[i])), 1)[0]
        title_num_2 = random.sample(range(len(train_pkl[numbers[x]])), 1)[0]
        first_title = train_pkl[i][title_num_1]
        second_title = train_pkl[numbers[x]][title_num_2]
        train_examples.append(InputExample(texts=[first_title, second_title], label=0.0))


for i in tqdm(test_class_id):
    # [TRAIN] sample 2 titles from same class_id
    numbers = random.sample(range(len(test_pkl[i])), 2)
    first_title = test_pkl[i][numbers[0]]
    second_title = test_pkl[i][numbers[1]]
    test_examples.append(InputExample(texts=[first_title, second_title], label=1.0))
    
    # [TEST]
    numbers = random.sample(range(len(test_pkl)), args.ratio)
    while (i in numbers):
        numbers = random.sample(range(len(test_pkl)), args.ratio)

    for x in range(args.ratio):
        title_num_1 = random.sample(range(len(test_pkl[i])), 1)[0]
        title_num_2 = random.sample(range(len(test_pkl[numbers[x]])), 1)[0]
        first_title = test_pkl[i][title_num_1]
        second_title = test_pkl[numbers[x]][title_num_2]
        test_examples.append(InputExample(texts=[first_title, second_title], label=0.0))


print("Final train example:", len(train_examples))
print("Final test example:", len(test_examples))

# Define your train dataset, the dataloader and the train loss
train_dataset = SentencesDataset(train_examples, model)
train_dataloader = DataLoader(train_dataset, shuffle=False, batch_size=32)
train_loss = losses.CosineSimilarityLoss(model)

evaluator = BinaryClassificationEvaluator.from_input_examples(test_examples, name='eval-dev')
model_save_path = args.out


print('Tune the model')
model.fit(train_objectives=[(train_dataloader, train_loss)],
          evaluator=evaluator,
          epochs=args.epochs,
          evaluation_steps=args.eval_steps,
          warmup_steps=100,
          output_path=model_save_path)

print('Finished tuning the model')
model.save(args.out + "_final_weights")


# these code below is needed because the produced model do not have tokenizer.json
# that is expected to have

import os
import shutil

shutil.copyfile(args.pre + '/tokenizer.json', args.out + '/0_Transformer/tokenizer.json')
shutil.copyfile(args.pre + '/tokenizer.json', args.out + '_final_weights/0_Transformer/tokenizer.json')
os.remove(args.out + '/0_Transformer/unigram.json')
os.remove(args.out + '_final_weights/0_Transformer/unigram.json')


import nltk
import torch
import numpy as np
import pandas as pd
import pickle
import faiss
import argparse

from tqdm import tqdm
from sentence_transformers import SentenceTransformer
from multiprocessing import Pool, Process


parser = argparse.ArgumentParser()
parser.add_argument(
    '--pre', type=str, default='input/shopee-roberta-small',
    help='Specify your pretrained model name'
)

parser.add_argument(
    '--weight_out', type=str, default='eval_weight/my_final_model',
    help='Specify your output location to store your model weights calculation on given dataset'
)

parser.add_argument(
    '--dataset', type=str, default='testset1.csv',
    help='The dataset where you want to measure your model precision'
)

args = parser.parse_args()

model = SentenceTransformer(args.pre)
sku_test = pd.read_csv(args.dataset, sep='\t')
sku_test = sku_test[sku_test['title'].notna()]
sku_test.reset_index(inplace=True)
sku_test

print("Start processing")

def magic_function(text):
    return model.encode(text, convert_to_tensor=True)

unnormalized_results = []
for i in tqdm(range(len(sku_test))):
    unnormalized_results.append(magic_function(sku_test['title'][i]))


with open(args.weight_out, 'wb') as handle:
    handle.write(pickle.dumps(unnormalized_results))

print("pickle weight successfully of size:", len(unnormalized_results))
print("model output dimension:", len(unnormalized_results[0]))

tmp_result = []
for i in tqdm(range(len(unnormalized_results))):
    x = np.array([np.array(unnormalized_results[i])]).astype(np.float32)
    faiss.normalize_L2(x)
    tmp_result.append(x)

results = []
for i in tqdm(range(len(unnormalized_results))):
    results.append(unnormalized_results[i].tolist())

results = np.array(results, dtype='float32')
faiss_search_index  = faiss.IndexFlatL2(len(unnormalized_results[0]))
faiss_search_index.add(results)
_, k_closest_points = faiss_search_index.search(results, 2)
print(k_closest_points[:30])

# ACCURACY CHECKING
accuracy = 0
index = []
label = []
wrong = []

for i in tqdm(range(len(k_closest_points))):
    left_index = k_closest_points[i][0]
    matched_index = k_closest_points[i][1:]
    matched_label = [sku_test['class_label'][i] for i in matched_index]
    
    if (sku_test['class_label'][left_index] in matched_label):
        accuracy += 1
    else:
        index.append(left_index)
        wrong.append(matched_index)

print("Overall accuracy: ", accuracy/len(results))

## Getting Started
Part 1 Slides: https://docs.google.com/presentation/d/1HfvYuQaP52YLh_f_qfr3e3DssijTKV73FqAiVF6LKEw/edit#slide=id.gbfdddaba3c_1_30

Part 2 Slides: https://docs.google.com/presentation/d/1P86rCD_QeesXzrcH3mXFkBOPQ5ta4Fjs/edit#slide=id.gcef3951e54_0_0

Ensure you have `input/` and `output/` before preceeding. Input folder supposed to contain models and datasets while output folder to check for results of the training.

### Data generation
Ensure you have your dataset (in our case: `similar_items.csv`) in your `input/`. Then, run `python3 data_generation.py`. You will observe a new train and validation dataset produced in your `input/` folder

### Training script
Run 
`python3 train.py -h` to get the list of arguments that this scripts accepts

```
usage: train.py [-h] [--pre PRE] [--out OUT] [--epochs EPOCHS] [--eval_steps EVAL_STEPS] [--ratio RATIO]
                [--sample_train SAMPLE_TRAIN] [--sample_test SAMPLE_TEST]

optional arguments:
  -h, --help            show this help message and exit
  --pre PRE             Specify your pretrained model name, your file is recommended to be in input folder
  --out OUT             Specify your output model name, your file output will be in output folder
  --epochs EPOCHS       Specify how many epochs your model run
  --eval_steps EVAL_STEPS
                        Run validation for every eval_steps training dataset. Put a very large value to remove eval step
  --ratio RATIO         Ratio negative examples to positive examples in integer
  --sample_train SAMPLE_TRAIN
                        Number of class_id you want to sample out of your train dataset (max_value)
  --sample_test SAMPLE_TEST
                        Number of class_id you want to sample out of your test dataset (max_value)
```

The model used MEAN pooling by default as it performs the best. It will then saved the model to your specified output folder and the end of the training.

<b>Example:</b>
```
python3 train.py 
    --pre='input/shopee-roberta-small'
    --out='output/small_mean_1_10_600k'
    --epochs=8
    --eval_steps=10000
    --ratio=10
    --sample_train=60000
    --sample_test=10000
```

<hr>

### Evaluation script
Run 
`python3 eval.py -h` to get the list of arguments that this scripts accepts

```
usage: eval.py [-h] [--pre PRE] [--weight_out WEIGHT_OUT] [--dataset DATASET]

optional arguments:
  -h, --help            show this help message and exit
  --pre PRE             Specify your pretrained model name
  --weight_out WEIGHT_OUT
                        Specify your output location to store your model weights
                        calculation on given dataset
  --dataset DATASET     The dataset where you want to measure your model precision
```

It will saved your model tensors based on the given dataset to your specified location. Then it prints out the model accuracy based on the given dataset.

<b>Example:</b>
```
python3 eval.py
    --pre="output/small_mean_1_10_600k"
    --weight_out="eval_weight/small_mean_1_10_600k_weight"
    --dataset="input/testset1.csv"
```

<hr>

### Encoding script
The structure on how to encode:
```
python3 encode.py <file_path> <model_path> <output_path>
```
`file_path` indicates your file_path where your sentences will be encoded by your model

`model_path` your model path

`output_path` your output pickle file (.pkl)

Note: If you directly call from the terminal, `file_path` can only accept a string, hence a path to a file. It cannot accept an iterable unless it calls from another script.

Example:
```
python3 encode.py ./test.txt ./input/my_trained_model ./out.pkl
```
